#include "mainwindow.h"
#include <QApplication>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <iostream>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <fstream>
#include <iomanip>

using namespace cv;
using namespace std;

std::string toBinary(int n)
{
    std::string r;
    while(n!=0) {r=(n%2==0 ?"0":"1")+r; n/=2;}
    return r;
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();



    std::string i= toBinary(8);

    std::ostringstream ss;
    ss << std::setw(3) << std::setfill('0') << i << "\n";
    std::string s2(ss.str());
    std::cout << s2;

/*
    QString dir_input="/home/boukary/movits-brazil/video-morocco/90-120/Toyota Auris(95)/";

    Mat frame;


    QDir dir(dir_input);
    //dir.setSorting(QDir::);
    QStringList filtre;
    filtre<<"*.bmp";
    QFileInfoList images_path;
    images_path = dir.entryInfoList(filtre,QDir::Files);


    cout << images_path.size()<<endl;
    QString chemin;
    for(int i=0;i<images_path.size();i++)
    {

        frame=imread(images_path.at(i).absoluteFilePath().toStdString());

        cvtColor(frame, frame, CV_BGR2GRAY);
        cvtColor(frame, frame, CV_BayerGB2BGR);

        QString path = images_path.at(i).absoluteFilePath();
        QStringList list = path.split("/");

        list[list.size()-1] = QString(format("image%05d.bmp",i+1).c_str());
        list[list.size()-2] = "RGB";

        path = list.join("/");
        imwrite(path.toStdString().c_str(),frame);
        //imshow("images",frame);
        //waitKey(1);
    }*/

    return a.exec();
}
