#-------------------------------------------------
#
# Project created by QtCreator 2017-10-03T10:09:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = convertimages
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui
INCLUDEPATH += '/usr/local/include'
